#encoding: utf-8
class Dragon
  def initialize name
    @name = name
    @asleep = false
    @stuffInBelly = 10
    @stuffInIntestine = 0

    puts @name + ' родился.'
  end

  def feed
    puts 'Вы кормите ' + @name + '(a).'
    @stuffInBelly = 10
    passageOfTime
  end

  def walk
    puts 'Вы выгуливаете ' + @name + '(a).'
    @stuffInIntestine = 0
    passageOfTime
  end

  def putToBed
    puts 'Вы укладываете ' + @name + ' (a) спать.'
    @asleep = true
    3.times do
      if @asleep
        passageOfTime
      end
      if @asleep
        puts @name + ' храпит, наполняя комнату дымом.'
      end
    end
    if @asleep
      @asleep = false
      puts @name + ' медленно просыпается.'
    end
  end

  def toss
    puts 'Вы подбрасываете ' + @name + ' (а) в воздух.'
    puts 'Он хихикает, обжигая при этом вам брови.'
    passageOfTime
  end

  def rock
    puts 'Вы нежно укачиваете ' + @name + ' (a). '
    @asleep = true
    puts 'Он быстро задрёмывает...'
    passageOfTime
    if @asleep
      @asleep = false
      puts '... но просыпается, как только вы перестали качать.'
    end
  end

  private
# "private" означает, что определённые здесь методы являются
# внутренними методами этого объекта. (Вы можете кормить
# вашего дракона, но не можете спросить его, голоден ли он.)
  def hungry? # голоден?
# Имена методов могут заканчиваться знаком "?".
# Как правило, мы называем так только, если метод
# возвращает true или false, как здесь:
    @stuffInBelly <= 2
  end

  def poopy? # кишечник полон?
    @stuffInIntestine >= 8
  end

  def passageOfTime # проходит некоторое время
    if @stuffInBelly > 0
      # Переместить пищу из желудка в кишечник.
      @stuffInBelly = @stuffInBelly - 1
      @stuffInIntestine = @stuffInIntestine + 1
    else # Наш дракон страдает от голода!
      if @asleep
        @asleep = false
        puts 'Он внезапно просыпается!'
      end
      puts @name + ' проголодался! Доведенный до крайности, он съедает ВАС!'
      exit # Этим методом выходим из программы.
    end
    if @stuffInIntestine >=10
      @stuffInIntestine = 0
      puts 'Опаньки! ' + @name + ' сделал нехорошо...'
    end
    if hungry?
      if @asleep
        @asleep = false
        puts 'Он внезапно просыпается!'
      end
      puts 'В желудке у ' + @name + '(a) урчит...'
    end

    if poopy?
      if @asleep
        @asleep = false
        puts 'Он внезапно просыпается!'
      end
      puts @name + ' подпрыгивает, потому что хочет на горшок...'
    end
  end
end

puts 'Назовите своего будущего питомца-дракончика'
name = gets.chomp
pet = Dragon.new name.to_s.capitalize
act = ''
while act!= 0
  puts 'Что Вы хотите сделать с дракончиком?:'
  puts '1-покормить, 2-выгулять, 3-уложить, 4-подбросить, 5-укачать, 0-выход'
  act= gets.chomp.to_i
  if act == 1
    pet.feed
  elsif act == 2
    pet.walk
  elsif act == 3
    pet.putToBed
  elsif act == 4
    pet.toss
  elsif act == 5
    pet.rock
  end
end




