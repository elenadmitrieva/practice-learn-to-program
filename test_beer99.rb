#encoding: utf-8
def englishNumber number
  if number < 0
    return 'Пожалуйста, введите неотрицательное число.'
  end
  if number == 0
    return 'zero'
  end
  numString = '' # Эту строку мы будем возвращать.
  onesPlace = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
  tensPlace = ['ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
  teenagers = ['eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
  # "left" - сколько от числа нам ещё осталось вывести.
  # "write" - часть числа, которую мы выводим сейчас.
  left = number
  write = left/1000000000
  left = left - write*1000000000
  if write >0
    milliard = englishNumber write
    numString = numString + milliard + ' milliard'
    if left >0
      numString = numString + ' '
    end
  end
  write = left/1000000
  left = left - write*1000000
  if write >0
    million = englishNumber write
    numString = numString + million + ' million'
    if left >0
      numString = numString + ' '
    end
  end
  write = left/1000
  left = left - write*1000
  if write >0
    thousand = englishNumber write
    numString = numString + thousand + ' thousand'
    if left >0
      numString = numString + ' '
    end
  end
  write = left/100
  left = left - write*100
  if write >0
    hundreds = englishNumber write
    numString = numString + hundreds + ' hundred'
    if left >0
      numString = numString + ' '
    end
  end
  write = left/10
  left = left - write*10
  if write>0
    if ((write == 1) and (left > 0))
# Поскольку мы не можем вывести "tenty-two" вместо "twelve",
      numString = numString + teenagers[left - 1]
      left = 0
    else
      numString = numString + tensPlace[write-1]
    end
    if left > 0
      numString = numString + '-'
    end
  end
  write = left
  left = 0
  if write > 0
    numString = numString + onesPlace[write-1]
  end
  numString
end
  count = gets.chomp.to_i
  while count != 0
    puts englishNumber(count).capitalize + ' бутылок пива на стене'
    puts englishNumber(count).capitalize + ' бутылок пива'
    puts 'Возьми одну, пусти по кругу'
    count-=1
  end
  puts 'Ни одной бутылки пива на стене'
