#encoding: utf-8
def maybeDo someProc
  if rand(2) ==0
    someProc.call
  end
end

def twiceDo someProc
  someProc.call
  someProc.call
end

wink = Proc.new do
  puts '<подмигнуть>'
end
glance = Proc.new do
  puts '<взглянуть>'
end
maybeDo wink
puts ''
maybeDo glance
puts ''
twiceDo wink
puts ''
twiceDo glance




