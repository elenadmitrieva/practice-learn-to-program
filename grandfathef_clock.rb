#encoding: utf-8
def grandfather someProc
  puts Time.now.hour.to_s
  time = Time.now.hour.to_i%12
  while (time!=0)
    someProc.call
    time-=1
  end
end

bom = Proc.new do
  puts 'Бом!'
end

grandfather bom