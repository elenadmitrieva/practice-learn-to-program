#encoding: utf-8
class Greeter
  def initialize(name)
    @name = name
  end
  def name
    @name
  end
  def name=(new_name)
    @name = new_name
  end
end
g = Greeter.new("Барни")
puts g.name #=> Барни
#g.name = "Бетти"
puts g.name #=> Бетти
