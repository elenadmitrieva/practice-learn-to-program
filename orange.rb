#encoding: utf-8
Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8
require 'rubygems'
require 'active_support/core_ext'
class OrangeTree
  def initialize
    @hgt = 70
    @year = 0
    @orangeCount = 0
  end

  def height
    puts @hgt.to_s + 'см'
  end

  def oneYearPasses
    @year+=1
    puts 'tree is ' + @year.to_s + ' years'
    @hgt+=30
    if @year == 10
      puts 'tree died'
      exit
    end
    countTheOranges
  end

  def pickAnOrange
    if (@orangeCount !=0)
      @orangeCount-=1
      if (@year<5)
        puts 'Вкусный апельсин'
      elsif (@year>5 and @year <9)
        puts 'Сладкий апельсин'
      elsif puts 'Сладчайший апельсин'
      end
      puts 'На дереве осталось '+ @orangeCount.to_s + ' апельсинов'
    else
      puts 'Нет апельсинов для сбора'
    end
  end

  private
  def countTheOranges
    if @year<2
      puts 'tree don\'t have fruits'
    else
      @orangeCount = 100

      if @year > 3
        @orangeCount+= 10*@year
      end
      puts @orangeCount.to_s + ' oranges on the tree'
    end
  end
end

tree = OrangeTree.new
act = ''
while act!= 0
  puts 'Ваше дерево:'
  puts '1-прошел год, 2-показать его рост, 3-собрать фрукты, 0-выход'
  act= gets.chomp.to_i
  if act == 1
    tree.oneYearPasses
  elsif act == 2
    tree.height
  elsif act == 3
    tree.pickAnOrange
  end
end




