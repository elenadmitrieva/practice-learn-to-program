#encoding: utf-8
class TestClass1
  colorArray = [] # то же, что Array.new
  colorHash = {} # то же, что Hash.new
  colorArray[0] = 'красный'
  colorArray[1] = 'зелёный'
  colorArray[2] = 'синий'
  colorHash['строки'] = 'красный'
  colorHash['числа'] = 'зелёный'
  colorHash['ключевые слова'] = 'синий'
  colorArray.each do |color|
    puts color
  end
  colorHash.each do |code, color|
    puts code + ': ' + color
  end
end

