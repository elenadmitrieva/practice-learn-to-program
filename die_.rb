#encoding: utf-8
class Die
  def initialize
    roll
  end

  def cheat
    puts 'What do you want?'
    dream = gets.chomp
    while (@numberShowing != dream.to_i)
      roll
    end
    @numberShowing
  end

  def roll
    @numberShowing = 1 + rand(6)
  end

  def showing
    @numberShowing
  end
end

obj1 = Die.new
puts obj1.cheat
obj1.roll
puts obj1.showing

=begin
puts Die.new.showing
puts Die.new.cheat
=end
